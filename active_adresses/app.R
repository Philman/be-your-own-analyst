#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#

library(shiny)
library(tidyverse) 
library(lubridate)
library(jsonlite)

assets <- fromJSON("https://community-api.coinmetrics.io/v2/assets")$assets
metrics <- fromJSON("https://community-api.coinmetrics.io/v2/metrics")$metrics
asset_info_raw <- fromJSON("https://community-api.coinmetrics.io/v2/asset_info")$assetsInfo[,1:3] %>% as_tibble()
asset_info <- asset_info_raw %>% select(-metrics)
asset_metrics <- asset_info_raw %>% unnest_longer(metrics)
# Idee: Bei einzelklicken wird das angesprochen, bei mehrfachwahl wird zu schon bestehenden daten was hinzugefügt
get_data <- function(asset,metrics) read_csv(paste0("https://community-api.coinmetrics.io/v2/assets/",asset,"/metricdata.csv?metrics=",metrics[1],",",metrics[2])) %>% mutate(Asset = asset) 

add_data <- function(first_data,asset,metrics) first_data %>% bind_rows(get_data(asset,metrics))

get_all_data <- function(assets,metrics){
    data <- get_data(assets[1],metrics)
    for(i in 2:length(assets))
        #print(i)
        data <- add_data(data,assets[i],metrics)
    data
}


#all_data %>% ggplot(aes(daa,prices,col=currs)) + geom_point() + scale_y_log10() + scale_x_log10() + theme_minimal() + labs(title="Price development vs active adresses", subtitle = "Printed on log-log-scale", caption = "Data from Coinmetrics.io", col = "Cryptocurrency", x = "Transaction count", y = "Price in US-Dollar")

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("Examine daily active adresses"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            selectInput('asset', 'Assets', asset_info$name, multiple=TRUE, selectize=FALSE,selected = "Bitcoin"),
            #selectInput('metric_1','x-metric',asset_metrics$metrics[asset_metrics$name == "Bitcoin"], selectize=FALSE,selected = "AdrActCnt"),
            #selectInput('metric_2','y-metric',asset_metrics$metrics[asset_metrics$name == "Bitcoin"], selectize=FALSE,selected = "PriceUSD")
        ),

        # Show a plot of the generated distribution
        mainPanel(
           plotOutput("plot")
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output,session) {

#    observe({
#        updateSelectInput(session, "metric_1", choices = asset_metrics$metrics[asset_metrics$name %in% input$asset])
#        updateSelectInput(session, "metric_2", choices = asset_metrics$metrics[asset_metrics$name %in% input$asset],selected = "PriceUSD")
#        })
    
    output$plot <- renderPlot({
        # generate bins based on input$bins from ui.R
        print(input$asset)
        if(length(input$asset)==1)
            daten <- get_data(asset_info$id[asset_info$name == input$asset],c("AdrActCnt","PriceUSD"))
        else {
            #assets <- asset_metrics$id[input$asset %in% asset_metrics$name]
            daten <- get_all_data(asset_info$id[asset_info$name %in% input$asset],c("AdrActCnt","PriceUSD"))
        }
        daten %>%ggplot(aes_string(names(daten)[2],names(daten)[3], col = names(daten)[4])) + 
            geom_point() + scale_x_log10("Active Adresses") + scale_y_log10("Price in US-Dollar") +
            theme_minimal()
    })
}

# Run the application 
shinyApp(ui = ui, server = server)
