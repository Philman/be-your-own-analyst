# Awesome Crypto-Apis

A curated list of API-services in the Bitcoin- and Altcoin-ecosystems

## Contents

- [General information](#general-information)
- [On-chain information](#on-chain-information)
  - [Bitcoin](#bitcoin)
  - [Ethereum](#ethereum)
  - [XRP](#xrp)
- [Other Crypto-related information](#other-crypto-related-information)
  - [ICOs, STOs etc.](#icos-stos-etc)
  - [DeFi-Stuff](#defi-stuff)
  
## General information

Beside APIs from various exchanges some sites such as coinmarketcap provide price information. Most of these sites focus on price- and volume-related metrics, albeit some of them also look on some on-chain-metrics.

- [Coinmarketcap](https://coinmarketcap.com/api/) - Coinmarketcap, while being the most well-known coin ranking site, falls short from a data-scientists viewpoint: Not only is an API key needed, the free tier does not provide andy historical data officially.

- [Coingecko](https://www.coingecko.com/api/documentations/v3) - Coingecko, while not as well-known as coinmarketcap, can be seen as a viable alternative with a lot of exchange-related data. Especially interesting is that historical market cap and historical trading volume are available. 

- [Coinpaprika](https://coinpaprika.com/api/) - On the first glance simply another Coinmarketcap-lookalike, with again a free API. Interesting things such as recent Tweets for a cryptocurrency or Informations about relevant celebrities. 

- [Coinmetrics](https://docs.coinmetrics.io/api/v2/) - The free community API provides interesting data both about price-development as well as some on-chain-metrics. Great for modeling stuff such as S2F etc. 

- [Cryptocompare](https://min-api.cryptocompare.com/) - Another Coinmarketcap-alternative which focuses heavily on community interaction (i.e. Reviews etc). Even in free tier quite exhaustive with full daily pricing history, Sentiment data, or news for specific coins. 

- [Coinograph](https://coinograph.io/realtime-api/) - Mainly great for getting current prices of assets at various exchanges, though historical data can be downloaded as a data dump. 

- [Coinapi](https://docs.coinapi.io/#Introduction) - Focus on exchange-related data such as exchange rates or order book data.

- [Chasing Coins](https://chasing-coins.com/api) - On the first glance a quite general API providing price information etc. but also a metric called ["Coin heat"](https://chasing-coins.com/market/opportunities) which should work as an indicator for upcoming price changes. Also some infos about ICOs are available. 

- [Coincap](https://docs.coincap.io/?version=latest) - Similar to coingecko and others but provides also supply information and a [volume-weighted average price (VWAP)](https://www.investopedia.com/terms/v/vwap.asp). 

- [Messari](https://messari.io/api/docs) - free tier is limited to 20 requests/minute. Registration is needed but all in all quite comprehensive list of metrics.  

- [Nomics](http://docs.nomics.com/)	- Professional API for investigating the price developments at various Crypto markets. Free tier available. 

- [Santiment](https://neuro.santiment.net/) - Free tier provides info about last three months excluding the last 48 hours. Focus on sentiment-based metrics (social volume, social sentiment etc.) but provides also info about on-chain behaviour. 

## On-Chain-Information

- [Blockchair](https://blockchair.com/api/docs) - Already the free version offers a lot of blockchain-related information about Bitcoin, Bitcoin Forks, Ethereum and numerous other cryptocurrencies. However, rather limited if it comes to demanding tasks.

### Bitcoin

- [Esplora](https://github.com/Blockstream/esplora/blob/master/API.md) - The API behind Blockstreams own Blockchain explorer. Provides various block- and transaction-related information

### Ethereum

- [Etherscan](https://etherscan.io/apis) - Etherscan is perhaps the most well-known explorer in the Ethereum-universe. Its API is quite thorough and can help to shine light on various blockchain-related data. 

- [Infura](https://infura.io/docs) - Infura is known as a very professional API providing in-depth insight into blockchain-related details. 

### XRP

- [XRP Ledger Dev Portal](https://developers.ripple.com/data-api.html) - The standard API for querying the XRP Ledger.

## Other crypto-related information

### ICOs, STOs etc.

- [Icobench](https://icobench.com/developers) - API to parse information about upcoming and past token sales. 

### DeFi-Stuff

- [aleth.io](https://developers.aleth.io/) - API to access Ethereum blockchain. Aleth.io itself has a strong focus on DeFi-Related data which can also be accessed via this API. ``

- [Defipulse](https://docs.defipulse.com/) - API with focus on DeFi-related data. 
