# Be your own analyst article series

Some articles about crypto-related analysis with R (in german tho)

- [Wie komme ich an gute Bitcoin-Daten](https://www.btc-echo.de/be-your-own-analyst-wie-komme-ich-an-gute-bitcoin-daten/)
- [Wie analysiere ich das On-Chain-Verhalten Bitcoins](https://www.btc-echo.de/wie-analysiere-ich-das-on-chain-verhalten-bitcoins/)
- [Bitcoin-Mining unter der Lupe](https://www.btc-echo.de/bitcoin-mining-unter-der-lupe/)
- [Ethereum-Peaks mit Santiment vorhersagen](https://www.btc-echo.de/ethereum-peaks-mit-santiment-vorhersagen/)